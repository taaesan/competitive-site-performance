//const rootUrl = 'https://master.isuzu-tis.com' //<-- No ending /
const rootUrl = 'https://toyota.co.th'//'https://staging.isuzu-tis.com'
const config = 
{
    timeoutConfig : 60000,
    launchConfig: { headless: false, args: ['--no-sandbox', '--disable-setuid-sandbox']},
    authenConfig: {username:'tr1petch', password:'bangk0k'},
    viewPortConfig: { width: 1800, height: 1000 },
    rawFile: './resources/raw.json',
    reportPath: './resources/Competitive_Analysis.xlsx',
    font: {
        name: 'Arial',
        family: 1,
        size: 12
    }
}
export {rootUrl, config}