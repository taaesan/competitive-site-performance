export interface Dealer {
  id: number;
  dealer_name: string;
  show_test_drive: number;
  show_financial: number;
  show_isp: number;
  sap_code_status: string;
  branch_status: string;
  province: string;
  district: string;
  expectedVaule?: string;
  isp_email_branch?:string;
  isp_email_hq?:string;
  email_test_drive?:string;
  email_tis_zone?:string;
  new_id?:string;
}
