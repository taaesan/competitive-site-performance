import fs from 'fs'
import {HeaderReport} from './performance/Report'
import Excel from 'exceljs'
import {config} from './config'

const csv = require('csvtojson')

module.exports = async function() {
    console.log('---Generating reports---')

    let jsonArray : HeaderReport[] = JSON.parse(fs.readFileSync(config.rawFile, 'utf8')) as HeaderReport[]

    let workbook : Excel.Workbook = new Excel.Workbook();

    generateWorksheet('Competitive Analysis', workbook, jsonArray, '')

    await workbook.xlsx.writeFile(config.reportPath)

}

function generateWorksheet(sheetName:string, workbook : Excel.Workbook, reports : HeaderReport[], filePath:string){
    
  let worksheet : Excel.Worksheet = workbook.addWorksheet(sheetName)
  worksheet.columns = [
      { header: 'url', key: 'url', width: 40 },
      { header: 'FCP', key: 'FCP', width: 10 },
      { header: 'FMP', key: 'FMP', width: 10 },
      { header: 'TTI', key: 'TTI', width: 10 },
      { header: 'Performance', key: 'Performance', width: 15 },
      { header: 'Accessibility', key: 'Accessibility', width: 15 },
      { header: 'Best Practices', key: 'BestPractices', width: 15 },
      { header: 'SEO', key: 'SEO', width: 15 }
    ];
    

    /*
      { "title": "First Contentful Paint", "displayValue": "3.7 s" },
      { "title": "First Meaningful Paint", "displayValue": "5.1 s" },
      { "title": "Time to Interactive", "displayValue": "14.0 s" },
      { "title": "Performance", "displayValue": "23 %" },
      { "title": "Accessibility", "displayValue": "56 %" },
      { "title": "Best Practices", "displayValue": "86 %" },
      { "title": "SEO", "displayValue": "92 %" }
    */

  for(let i = 0; i < reports.length; i++) {
      let header:HeaderReport = reports[i]

      let row:Excel.Row = worksheet.addRow({
          url: header.site, 
          FCP: header.reports[0].displayValue, 
          FMP: header.reports[1].displayValue, 
          TTI: header.reports[2].displayValue, 
          Performance: header.reports[3].displayValue, 
          Accessibility: header.reports[4].displayValue, 
          BestPractices: header.reports[5].displayValue, 
          SEO: header.reports[6].displayValue
      })
      row.font = config.font
  }
}
