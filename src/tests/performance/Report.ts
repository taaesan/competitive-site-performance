export interface DetailReport {
    title:string
    displayValue:string
}

export class HeaderReport {
    constructor(){
        this.reports = []
    }
    site: string = ''
    reports: DetailReport[] = []
}

