
const lighthouse = require('lighthouse/lighthouse-core')
import puppeteer from "puppeteer";
import {config} from '../config'
import {params} from './01.multisites.param'
import * as chromeLauncher from 'chrome-launcher';
import fs from 'fs'
import {HeaderReport, DetailReport} from './Report'

jest.setTimeout(config.timeoutConfig)

describe('Competitive Analysis', () => {
    let page : puppeteer.Page = null
    let browser : puppeteer.Browser = null
    const options = { chromeFlags: ['--headless','--no-sandbox'], port:0 }
    let headerReports:HeaderReport[] = []

    beforeAll(async () => {
        
        
    }) 

    test.each(params)(` '%s' `, async (url: string) => {
        console.log(`Analysing `+url)
        const chrome: chromeLauncher.LaunchedChrome = await chromeLauncher.launch({ chromeFlags: options.chromeFlags });
        options.port = chrome.port;

        const results = await lighthouse(url, options);
        const headerReport: HeaderReport = new HeaderReport()
        headerReport.site = url

        let reportRef: DetailReport = {title: results.lhr.audits['first-contentful-paint'].title, displayValue: results.lhr.audits['first-contentful-paint'].displayValue}
        headerReport.reports.push(reportRef)

        reportRef = {title: results.lhr.audits['first-meaningful-paint'].title, displayValue: results.lhr.audits['first-meaningful-paint'].displayValue}
        headerReport.reports.push(reportRef)

        reportRef = {title: results.lhr.audits['interactive'].title, displayValue: results.lhr.audits['interactive'].displayValue}
        headerReport.reports.push(reportRef)

        reportRef = {title: results.lhr.categories['performance'].title, displayValue: `${Math.round(results.lhr.categories['performance'].score * 100)} %`}
        headerReport.reports.push(reportRef)

        reportRef = {title: results.lhr.categories['accessibility'].title, displayValue: `${Math.round(results.lhr.categories['accessibility'].score * 100)} %`}
        headerReport.reports.push(reportRef)

        reportRef = {title: results.lhr.categories['best-practices'].title, displayValue: `${Math.round(results.lhr.categories['best-practices'].score * 100)} %`}
        headerReport.reports.push(reportRef)

        reportRef = {title: results.lhr.categories['seo'].title, displayValue: `${Math.round(results.lhr.categories['seo'].score * 100)} %`}
        headerReport.reports.push(reportRef)
        

        headerReports.push(headerReport)

        //fs.writeFileSync('report.json', results.report, 'utf8')
        await chrome.kill()
    })

    afterAll(async () => {
        fs.writeFileSync(config.rawFile, JSON.stringify(headerReports), 'utf8')
    })
    
  }) 
