module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  'roots': [
    '<rootDir>/src'
  ],
  // globalSetup: '<rootDir>/src/tests/setup.ts',
  globalTeardown: '<rootDir>/src/tests/teardown.ts',
  testMatch: [
    '<rootDir>/src/tests/performance/01.multisites.test.ts'
  ],
  testSequencer: '<rootDir>/src/tests/TestSort.js'
};